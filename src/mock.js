var mw = {};

if ( process.env.NODE_ENV === 'production' ) {
  mw = window.mw;
} else {
  mw = {
    config:{
      values:{
        WSSearchFront:{
          config:{
            "settings":{
              "size":2,
              "title":{
                "name":"Title",
                "key":633,
                "type":"wpgField",
                "label":"mainh"
              },
              "layout":"table"
            },
            "facetSettings":{
              "Version":{
                "display":"range",
                "max": 5,
                "step": 1,
                "label": "soho"
              },
              "Title":{
                "display":"combobox",
                "label":"tttstst"
              },
              "Date":{
                "display":"datepicker",
                "label":"dd"
              }
            },
            "hitSettings":{
              "Version":{
                "key":635,
                "display":"pill",
                "type":"wpgField",
                "label":"Testlabelll"
              },
              "Title":{
                "display":"link",
                "key":633,
                "type":"wpgField",
                "highlight":"true"
              }
            }
          }
        }
      }
    }
  };

  class Api{
    post(){
      var res = new Promise((resolve) => {
        return resolve({
          "result":{
            "hits":"[{\"_index\":\"smw-data-wiki-v2\",\"_type\":\"data\",\"_id\":\"670\",\"_score\":1,\"_source\":{\"subject\":{\"title\":\"Ytdytd\",\"subobject\":\"\",\"namespace\":0,\"interwiki\":\"\",\"sortkey\":\"ds test\",\"serialization\":\"Ytdytd#0##\",\"sha1\":\"189866977a7ef400f4de8685e0e1b3c36fa66a61\",\"rev_id\":483,\"namespacename\":\"\"},\"P:631\":{\"txtField\":[\"Page\"]},\"P:671\":{\"wpgField\":[\"4000\"],\"wpgID\":[672]},\"P:633\":{\"wpgField\":[\"Test test\", \"Test meer\"],\"wpgID\":[674]},\"P:16\":{\"txtField\":[\"ds test\"]},\"P:29\":{\"datField\":[2459251.120787],\"dat_raw\":[\"1\\/2021\\/2\\/5\\/14\\/53\\/56\\/0\"]},\"text_raw\":\"Title=test test|namespace=4000\\n\"},\"highlight\":{\"text_raw\":[\"<b>Title<\\/b>=<b>test<\\/b> <b>test<\\/b>|<b>namespace<\\/b>=<b>4000<\\/b>\"]}},{\"_index\":\"smw-data-wiki-v2\",\"_type\":\"data\",\"_id\":\"642\",\"_score\":1,\"_source\":{\"subject\":{\"title\":\"Page3\",\"subobject\":\"\",\"namespace\":0,\"interwiki\":\"\",\"sortkey\":\"test test zwaar\",\"serialization\":\"Page3#0##\",\"sha1\":\"70fd004b9cf028444c9bcda2b9ded167b5713e7f\",\"rev_id\":424,\"namespacename\":\"\"},\"P:631\":{\"txtField\":[\"Page\"]},\"P:671\":{\"wpgField\":[\"4000\"],\"wpgID\":[672]},\"P:633\":{\"wpgField\":[\"Test test zwaar\"],\"wpgID\":[643]},\"P:635\":{\"wpgField\":[\"2\"],\"wpgID\":[636]},\"P:16\":{\"txtField\":[\"test test zwaar\"]},\"P:29\":{\"datField\":[2459243.4691898],\"dat_raw\":[\"1\\/2021\\/1\\/28\\/23\\/15\\/38\\/0\"]},\"text_raw\":\"\\n\\n\\n\"}}]",
            "total":9,
            "aggs":
                {
                  "Version":{
                    "doc_count_error_upper_bound":0,
                    "sum_other_doc_count":0,
                    "buckets":[
                      {
                        "key":"2",
                        "doc_count":3
                      },
                      {
                        "key":"1",
                        "doc_count":1
                      },
                      {
                        "key":"3",
                        "doc_count":1
                      }
                    ]
                  },
                  "Title":{
                    "doc_count_error_upper_bound":0,
                    "sum_other_doc_count":0,
                    "buckets":[
                      {
                        "key":"Hoi hoi",
                        "doc_count":1
                      },
                      {
                        "key":"Hoi iyf la tra water",
                        "doc_count":1
                      },
                      {
                        "key":"Test nog",
                        "doc_count":1
                      },
                      {
                        "key":"Test test",
                        "doc_count":1
                      },
                      {
                        "key":"Test test zwaar",
                        "doc_count":1
                      }
                    ]
                  },
                  "Date":{
                    "buckets":[
                      {
                        "key":"2017",
                        "from":2457754,
                        "to":2458119,
                        "doc_count":0
                      },
                      {
                        "key":"2018",
                        "from":2458119,
                        "to":2458484,
                        "doc_count":0
                      },
                      {
                        "key":"2019",
                        "from":2458484,
                        "to":2458849,
                        "doc_count":0
                      },
                      {
                        "key":"2020",
                        "from":2458849,
                        "to":2459215,
                        "doc_count":0
                      },
                      {
                        "key":"Last Quarter",
                        "from":2459145,
                        "to":2459268,
                        "doc_count":4
                      },
                      {
                        "key":"2021",
                        "from":2459215,
                        "to":2459580,
                        "doc_count":5
                      },
                      {
                        "key":"Last Month",
                        "from":2459237,
                        "to":2459268,
                        "doc_count":4
                      },
                      {
                        "key":"Last Week",
                        "from":2459261,
                        "to":2459268,
                        "doc_count":0
                      }
                    ]
                  }
                }
          }
        });
      })
      res.done = res.then;
      return res;
    }
  }
  mw.Api = Api;
}

window.mw = mw;

export default mw;

