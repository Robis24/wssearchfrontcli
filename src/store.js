import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

var routing = false; 

var updateStore = function(store){
  store.subscribe(function(mutation, state){
      if(mutation.type === "START" 
      || mutation.type === "SET_TERM" 
      || mutation.type === "SET_FROM" 
      || mutation.type === "SET_SELECTED" ){


          if(mutation.type != "SET_FROM"){
            state.from = 0;
          }

          //mount function need to move this to somewhere else
          if(mutation.type === "START"){

              //create dates
              var today = new Date();
              var lastWeek = new Date();
              lastWeek.setDate( lastWeek.getDate() - 7 );
              var lastMonth = new Date();
              lastMonth.setMonth( lastMonth.getMonth() - 1 );
              var lastQuarter = new Date();
              lastQuarter.setMonth( lastQuarter.getMonth() - 4 );

              var readableToday = readableDate(today);

              var date_ranges = [];
              var real_date_ranges = {};

              real_date_ranges["Last Week"] = {"from": readableDate(lastWeek), "to":  readableToday};
              real_date_ranges["Last Month"] = {"from": readableDate(lastMonth), "to": readableToday };
              real_date_ranges["Last Quarter"] = {"from": readableDate(lastQuarter), "to": readableToday };

              for (var i=0; i < 5; i++) {
                var key = today.getFullYear() - i;
                real_date_ranges[key] = {"from": key + '-1-1' , "to": (key + 1) + '-1-1' };
              }

              Object.entries(real_date_ranges).forEach(function([key, value]){
                date_ranges.push({
                  "key":key.toString(),
                  "from":createDates(value.from),
                  "to" :createDates(value.to)
                });
              })


              var facetranges = [{
                "type": "range",
                "ranges": date_ranges,
                "property": "Modification date"
               }]

              
              state.realDates = real_date_ranges;


               //add more ranges

               var rangeprop = [];
                   // eslint-disable-next-line no-undef
               var facetSettings = window.mw.config.values.WSSearchFront.config.facetSettings
               Object.keys(facetSettings).forEach(function(key){
        
                   if(facetSettings[key].display == "range"){
                    facetSettings[key].name = key;
                    rangeprop.push(facetSettings[key])
                   }
        
        
               })

               if(rangeprop.length > 0){
                 rangeprop.forEach(function(prop){

                  var max = parseInt(prop.max);
                  var step = parseInt(prop.step);

 
                  var pessa = []
                  for (var i = 1; i < max + 1 ; i += step) {
                 

                     // pessa.push( { "from": i, "to": i + step, "key": (i + step) + '' } )
                  pessa.push( { "from": i , "to": max + 1, "key": (i + step - 1) + '' } )


                      
                  }


                  facetranges.push({
                    "type": "range",
                    "ranges": pessa,
                    "property": prop.name
                   })

          
                })
               }

          

               state.dates = facetranges;



              //get from url string

              var queryString = window.location.search;
              var urlParams = new URLSearchParams(queryString);
              var page = urlParams.get('title');
              if(page){
                    routing = page;

              }




              var term = urlParams.get('term');
              var offset = urlParams.get('offset');
              var filters = urlParams.get('filters');

              if(term){
                state.term = term;
              }

              if(offset){
                state.from = parseInt(offset);
              }


              if(filters){

                var urlFiltersOutput = [];
                var activeFilters =  filters.split("~~");

                activeFilters.forEach(function(value) {

                  var filterItem = value.split("^^");
                  var rangeItem = filterItem[0].split("_");

                  if(rangeItem[0] == "range"){

                    var ranges = filterItem[1].split("_");

                    if(rangeItem[1] == "customrange"){

                      Vue.set(
                          state.realDates,
                          "customrange", 
                          {
                            "from": ranges[0], 
                            "to":  ranges[1]
                          }
                      )  

                    }

                    if(rangeItem[1] == "customrange" || rangeItem[1] == "date" || rangeItem[1] == "Modification date" ){

                        state.rangeFrom = createDates(ranges[0]);
                        state.rangeTo = createDates(ranges[1]);

                        urlFiltersOutput.push(
                          {
                            key:rangeItem[2],
                            value:rangeItem[1],
                            range: {
                                gte: state.rangeFrom,
                                lte:state.rangeTo
                              } 
                          }
                        );

                    }else{

                      urlFiltersOutput.push(
                        {
                          key:rangeItem[2],
                          value:rangeItem[1],
                          range: {
                              gte: createDates(ranges[0]),
                              lte:createDates(ranges[1])
                           } 
                        }
                      );
                    }
                  
                  }else{

                    urlFiltersOutput.push(
                      {
                        value:filterItem[1],
                        key: filterItem[0] 
                      }
                    );
                  }
              });
              state.selected = urlFiltersOutput;
            }
          }


          //api call, this can stay here

          var root = state;
          root.loading = true;

          var urlsytr = urlstring();
          var routingString = "";
          if(routing){
            routingString = '/' + routing
             routing = false;
          }
          console.log( routingString)

          window.history.replaceState('', '', urlsytr )


          var params = {
            action: 'query',
            meta: 'WSSearch',
            format: 'json',
            filter: JSON.stringify(root.selected),
            term:root.term,
            from:root.from,
            limit:root.size,
                  // eslint-disable-next-line no-undef
            pageid:mw.config.values.wgArticleId,
            aggregations:JSON.stringify(root.dates)
          },
              // eslint-disable-next-line no-undef
          api = new mw.Api();

          api.post(  params ).done( function ( data ) {
            root.total = data.result.total;
            root.hits = JSON.parse(data.result.hits);
            root.aggs = data.result.aggs;
            root.loading = false;
          })
      }

      function urlstring(){
          //create url string

          var url = new URL(window.location.href)
          var searchParams = url.searchParams;

          state.from > 0 ? searchParams.set('offset', state.from) :  searchParams.delete('offset');
      
          state.term ? searchParams.set('term', state.term) : searchParams.delete('term');

          if(state.selected.length){
            var filtersArray = [];

            state.selected.forEach(function(item){
                if(item.range){
                  if(state.realDates[item.value]){
                   filtersArray.push("range_"+item.value+"_"+item.key+"^^"+state.realDates[item.value].from+"_"+state.realDates[item.value].to);
                  }else{
                    filtersArray.push("range_"+item.value+"_"+item.key+"^^"+item.range.gte+"_"+item.range.lte);
                  }
                }else{
                   filtersArray.push(""+item.key+"^^"+item.value+"");
                }
            });

            var filtersUrl = filtersArray.join("~~");
            searchParams.set('filters', filtersUrl);

          }else{
            searchParams.delete('filters');
          }

          return url;
      }

      function createDates(date){

        function parseDate(str) {
            return new Date(str)
        }

        function dateDiff(first, second) {
            return Math.round((second-first)/(1000*60*60*24));
        }

        // 2451544 = 2000- 1- 1
        var timestamp = 2451544;
        return dateDiff(parseDate('2000-01-01'), parseDate(date)) + timestamp;

    }

  })
}

function readableDate(date){
  return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
}


var store = new Vuex.Store({
  state:{
    loading:false,
    selected:[],
    hits: "",
    aggs: "",
      // eslint-disable-next-line no-undef
    size: parseInt(window.mw.config.values.WSSearchFront.config.settings.size),
    total:0,
    from: 0,
    rangeFrom: 0,
    rangeTo: 0,
    term: "" ,
    loaded:false,
    dates:[],
    realDates:{},
  },
  mutations:{
     SET_SELECTED(state, selected){
       state.selected = selected;
     },
     SET_TERM(state, term){
       state.term = term
     },
     SET_FROM(state, from){
       state.from = from
     },
     START(state, start){
       state.loaded = start;
     },
     SET_RANGE_from(state, range){
          state.rangeFrom = range
     },
     SET_RANGE_to(state, range){
      state.rangeTo = range
     },
     SET_REAL_DATES(state, date){
        state.realDates = date
     }
  },
  getters:{
     rangeFrom(state){
       return state.rangeFrom;
     },
     rangeTo(state){
      return state.rangeTo;
    }
  },
  plugins: [updateStore]

})

export default store
