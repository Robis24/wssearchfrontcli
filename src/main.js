import Vue from 'vue'
import i18n from 'vue-banana-i18n';
import mw from './mock';
window.mw = mw;

import App from './App.vue'

const messages = {
  en: {
    'clear-all-filters': 'Clear all filters',
    "search-total": "{{PLURAL:$1|result|results|0=No results}}",
    'date-range-from': 'Date from',
    'date-range-to': 'Date to',
    'wssearch-more': 'Show more',
    'wssearch-less': 'Show less'
  },
  nl: {
    'clear-all-filters': 'wis alle filters',
    "search-total": "{{PLURAL:$1|$1 resultaat|$1 resultatent|0=Geen resultaten}}",
    'date-range-from': 'Datum vanaf',
    'date-range-to': 'Datum tot'
  }
}

Vue.use(i18n, {
  locale:'en',
  messages:messages
})


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
