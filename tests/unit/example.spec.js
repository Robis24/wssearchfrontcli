import { shallowMount } from '@vue/test-utils'
import PillItem from '@/components/PillItem.vue'
import PillsSelected from '@/components/PillsSelected.vue'
import i18n from 'vue-banana-i18n'
import Vue from 'vue'
import mw from '@/mock.js'

import store from '@/store.js'

window.mw = mw

const messages = {
  en: {
    'hello_world': 'Hello world',
    'search_results': 'Found $1 {{PLURAL:$1|result|results}}',
    'profile_change_message': '$1 changed {{GENDER:$2|his|her}} profile picture'
  },
  ml: {
    'hello_world': 'എല്ലാവർക്കും നമസ്കാരം',
    'search_results': '{{PLURAL:$1|$1 ഫലം|$1 ഫലങ്ങൾ|1=ഒരു ഫലം}} കണ്ടെത്തി',
    'profile_change_message': '$1 {{GENDER:$2|അവന്റെ|അവളുടെ}} പ്രൊഫൈൽ പടം മാറ്റി'
  }
}

Vue.use(i18n, {
  locale:'es',
  messages:messages
})


describe('PillItem.vue', () => {
  it('renders props.activefilter when passed', () => {
    const activefilter = {key:'date', name:'200', value:"zzz"}
    const wrapper = shallowMount(PillItem, {
      propsData: { activefilter }
    })
    expect(wrapper.text()).toMatch("zzz")
  })
})

describe('PillsSelected.vue', () => {
  it('renders props.selected when passed', () => {
    const wrapper = shallowMount(PillsSelected, store,  {
    })
    expect(wrapper.contains('span')).toBe(true)
  })
})
